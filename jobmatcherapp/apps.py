from django.apps import AppConfig


class JobmatcherappConfig(AppConfig):
    name = 'jobmatcherapp'

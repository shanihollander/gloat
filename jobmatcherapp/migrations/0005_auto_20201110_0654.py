# Generated by Django 3.1.3 on 2020-11-10 06:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobmatcherapp', '0004_auto_20201110_0653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='candidate',
            field=models.ForeignKey(default='1', on_delete=django.db.models.deletion.CASCADE, to='jobmatcherapp.candidate'),
            preserve_default=False,
        ),
    ]

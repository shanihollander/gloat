# Generated by Django 3.1.3 on 2020-11-11 04:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobmatcherapp', '0014_auto_20201110_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidate',
            name='is_cur_employee',
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
    ]

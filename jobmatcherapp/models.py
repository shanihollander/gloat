from django.db import models


class MasterTitle(models.Model):
    id = models.TextField(primary_key=True)

    name = models.TextField()


class Title(models.Model):
    name = models.TextField(primary_key=True)

    master = models.ForeignKey(MasterTitle,
                               null=True,
                               on_delete=models.SET_NULL)


class Candidate(models.Model):
    id = models.TextField(primary_key=True)

    title = models.ForeignKey(Title,
                              on_delete=models.SET_NULL,
                              to_field="name",
                              null=True)

    # title = models.TextField()

    first_name = models.TextField()

    last_name = models.TextField()

    is_cur_employee = models.BooleanField()


class Skill(models.Model):
    name = models.TextField()

    candidate = models.ForeignKey(Candidate,
                                  on_delete=models.CASCADE)


class Job(models.Model):
    id = models.TextField(primary_key=True)

    title = models.OneToOneField(Title,
                                 on_delete=models.SET_NULL,
                                 to_field="name",
                                 null=True,
                                 related_name="title_master",
                                 related_query_name="title_master")

    required_skill = models.TextField()

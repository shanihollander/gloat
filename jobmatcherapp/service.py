from .models import Candidate, Job


def get_candidates_by_master_title(master_id):
    # candidates_raw = Skill.objects.raw("SELECT skl.id as sklid, cand.id as id , cand.first_name, "
    #                                    "cand.last_name, cand.title, skl.name as skill "
    #                                    "FROM public.jobmatcherapp_candidate as cand "
    #                                    "left join public.jobmatcherapp_skill as skl on skl.candidate_id = "
    #                                    "cand.id "
    #                                    "where title in (select title.name from public.jobmatcherapp_title as "
    #                                    "title "
    #                                    "where title.master_id = '" + master_title + "')")

    candidates_raw = Candidate.objects.filter(title__master=master_id)

    return candidates_raw


def get_job_by_id(job_id):
    # jobs = Job.objects.raw("SELECT job.id, job.title, job.required_skill, title.master_id" +
    #                        " FROM public.jobmatcherapp_job as job" +
    #                        " left join public.jobmatcherapp_title as title on job.title = title.name" +
    #                        " WHERE job.id = '" + str(job_id) + "'")

    job = Job.objects.get(id=str(job_id))

    formatted_job = {"id": job_id,
                     "required_skill": job.required_skill,
                     "title": job.title.name,
                     "master_id": job.title.master.id}

    return formatted_job


def get_all_jobs():
    return Job.objects.all()
from django.urls import path

from . import views

urlpatterns = [

    path("<int:pk>/", views.candidates_list, name="candidates_list"),
    path("", views.jobs, name="jobs")

]

from django.shortcuts import render
from .service import get_candidates_by_master_title, get_job_by_id, get_all_jobs


def jobs(request):
    context = {
        "jobs": get_all_jobs()
    }
    return render(request, "jobs.html", context)


def candidates_list(request, pk):
    job = get_job_by_id(pk)

    candidates = []
    if job and job["master_id"] is not None:
        candidates_raw = get_candidates_by_master_title(job["master_id"])
        candidates = process_data(candidates_raw, job)

    context = {

        "candidates": candidates,

        "job": job

    }

    return render(request, "candidates_list.html", context)


def process_data(candidates_raw, job):
    candidates = []
    NUM_OF_PARAMS = 3

    for candidate in candidates_raw:
        skills = list(candidate.skill_set.all().values_list("name", flat=True))
        match_percent = 1/NUM_OF_PARAMS
        if job["required_skill"] in skills:
            match_percent += 1/NUM_OF_PARAMS
        if candidate.is_cur_employee:
            match_percent += 1/NUM_OF_PARAMS

        candidates.append({'first_name': candidate.first_name,
                           'last_name': candidate.last_name,
                           'title': candidate.title.name,
                           'skills': candidate.skill_set.all().values_list("name", flat=True),
                           'match_percent': round(match_percent * 100, 2),
                           'is_cur_employee': candidate.is_cur_employee
                           })

    candidates.sort(key=by_matched_skill, reverse=True)
    return candidates


def by_matched_skill(candidate):
    return candidate['match_percent']
